package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid input for palindrome", Palindrome.isPalindrome("Anna"));
	}
	
	@Test
	public void testIsPalindromeBoundaryIn() {
		assertTrue("Invalid input for palindrome", Palindrome.isPalindrome("a"));
	}
	
	@Test
	public void testIsPalindromeBoundaryOut() {
		assertFalse("Invalid input for palindrome", Palindrome.isPalindrome("lab"));
	}
	
	@Test
	public void testIsPalindromeException() {
		assertFalse("Invalid input for palindrome", Palindrome.isPalindrome("Aaron"));
	}

}
