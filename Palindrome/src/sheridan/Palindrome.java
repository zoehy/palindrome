package sheridan;

public class Palindrome {
	
	public static boolean isPalindrome(String s) {
		s = s.toLowerCase().replaceAll(" ", "");
		for(int i = 0, j = s.length() - 1; i < j; i++, j--)
		{
			if (s.charAt(i) != s.charAt(j))
			{
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(" is [anna] palindrome? " + isPalindrome("anna"));
		System.out.println(" is [race car] palindrome? " + isPalindrome("race car"));
		System.out.println(" is [taco cat] palindrome? " + isPalindrome("taco cat"));
		System.out.println(" is [this is not palindrome] palindrome? " + isPalindrome("this is not palindrome"));
	}
}